# Some Message App

## Simple goal

Implement a simple server, which stores one message in memory (it is blank upon startup). Expose one and only one endpoint, the `/message` enpoint, which can receive GET and POST requests.

The GET request, returns a STATUS 200 and a JSON body with the form:

```
{
  message: [the current stored message]
}
```

The POST request will store a new message, it receives a body with the same format as above, upon successful storage of the new message in memory, it returns a 200 status code and a text stating "Message stored to memory."

To make it clear: **only one messsage exists in memory.** The GET request gets it from the server, the POST **overwrites whichever message is currently in existence.**

All other methods or endpoints should gracefully return a 405 status code, with empty body.

For the POST payload, if it does not conform to the above spec, it should return a 422 status code, with empty body.

For user stories, and CURLs to test the above, see the next section.

## Stories and Testing Requests

>As a developer  
>I want to implement a GET request to the `/message` endpoint  
>So that it responds with the currently stored message in memory  
>And a 200 response status code  

`curl http://localhost:3000/message`  

>As a developer  
>I want to implement a POST request to the /message endpoint  
>With a JSON of the form `{ message: [string] }` as payload  
>So that it responds with a 200 response status code  
>And a “Message stored to memory.” text payload  

`curl -ik --header 'Content-Type: application/json' -X POST --data '{ "message": "Hello dude!" }' http://localhost:3000/message`  

>As a developer  
>I want to add logic to the POST request to the /message endpoint  
>So that a payload other than a `{ message: [string] }` JSON responds with 422 unprocessable entity code  

`curl -ik --header 'Content-Type: application/json' -X POST http://localhost:3000/message`  
`curl -ik --header 'Content-Type: application/json' -X POST --data '{ "m": "Hello dude!" }' http://localhost:3000/message`  
`curl -ik -X POST --data '{ “message": "Hello dude!" }' http://localhost:3000/message`  

>As a developer  
>I want to implement a "catch all" logic to requests of any type to an endpoint other than `/message`  
>Or a request type other than GET or POST to the `/message` endpoint  
>So that the application responds with a 405 not allowed status code  

`curl -ikX PUT http://localhost:3000/message`  
`curl -ik http://localhost:3000`  
`curl -ik http://localhost:3000/message/something`  
`curl -ik --header 'Content-Type: application/json' -X POST --data '{ "message": "Hello dude!" }' http://localhost:3000/message/something`  

